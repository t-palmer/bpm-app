import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


import { AppComponent } from './app.component';
import { BpmConnectionConfigurationComponent }  from './bpm-connection-configuration';
import { ConnectionManagerService } from './connection-manager.service';
import { BpmComponentsModule, BpmModule } from 'ng-bpm';

@NgModule({
  declarations: [
    AppComponent,
	BpmConnectionConfigurationComponent
  ],
  imports: [
  BpmModule,
      NgbModule.forRoot(),
      BrowserModule,
      FormsModule,
      ReactiveFormsModule,
  BpmComponentsModule
  ],
  providers: [ConnectionManagerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
