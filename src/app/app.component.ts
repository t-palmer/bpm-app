import {Component, OnInit} from '@angular/core';
import { BpmService} from 'ng-bpm';//'../../ng-bpm/bpm.service';
import { BpmConnectionApplication } from 'ng-bpm';//'../../ng-bpm/bo/bpm-connection';
import { BpmWorkItem } from 'ng-bpm';//'../../ng-bpm/bo/bpm-work-item';
import { BpmWorkItemDetail } from 'ng-bpm';//'../../ng-bpm/bo/bpm-work-item-detail';
import { BpmProcessInstanceDetail } from 'ng-bpm';//'../../ng-bpm/bo/bpm-process-instance-detail';
import { BpmProcessDefinitionDetail } from 'ng-bpm';//'../../ng-bpm/bo/bpm-process-definition-detail';
import { WorkItemFilter } from 'ng-bpm';//'../../ng-bpm/bo/bpm-constants-wi';
import { BpmApplication } from 'ng-bpm';//'../../ng-bpm/bo/bpm-application';
import { Observable } from 'rxjs/Observable';
//import { RefErrorService } from 'ng-bpm';// '../ref-error.service';

import {TranslateService} from 'ng-bpm';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'bpm-app';
   wiArray: BpmWorkItem[] = [];
   sortBy: any = 'instanceName';
   sortOrder: any = false;
   
	private bpmApplicationConnection: BpmConnectionApplication;
	
  constructor(
        private translate: TranslateService,
		public bpmService: BpmService
    ) {
		this.translate.addLangs(['en', 'fr']);
        this.translate.setDefaultLang('en');
        const browserLang = translate.getBrowserLang();
        translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
		}
	
	ngOnInit(): void {
		
    }
	
	workItemClicked(event: BpmWorkItem) {
        console.log('Work Item bpmClick', event);
    }
	
  loadBpmData(bpmApplication: BpmApplication): void {
        const self = this;
	this.bpmApplicationConnection = bpmApplication.applicationConnection;
        this.bpmService
            .getWorkItemList(this.bpmApplicationConnection, WorkItemFilter.AllWorkItems)
            .subscribe(
                function(wis) {
                    self.wiArray = wis;
                }
            );
    }
}
